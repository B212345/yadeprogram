# basic simulation showing sphere falling ball gravity,
# bouncing against another sphere representing the support

# DATA COMPONENTS

# add 2 particles to the simulation
# they the default material (utils.defaultMat)

O.bodies.append(
        [
                # fixed: particle's position in space will not change (support)
                sphere(center=(0, 0, 0), radius=0.5, fixed=True),	
                # this particles is free, subject to dynamics
                sphere((0, 0, 2), 0.5)
        ]
)
# the above is as follows:	O.bodies.append([sphere(center=(0,0,0), radius=0.5, fixed=True),sphere(center=(0,0,2), radius=0.5). You can omit "center" and "radius" in the code.


# FUNCTIONAL COMPONENTS

# simulation loop -- see presentation for the explanation
O.engines = [
        ForceResetter(),
        InsertionSortCollider([Bo1_Sphere_Aabb()]),
        InteractionLoop(
                [Ig2_Sphere_Sphere_ScGeom()],  # collision geometry
                [Ip2_FrictMat_FrictMat_FrictPhys()],  # collision "physics"
                [Law2_ScGeom_FrictPhys_CundallStrack()]  # contact law -- apply forces
        ),
        # Apply gravity force to particles. damping: numerical dissipation of energy.
        NewtonIntegrator(gravity=(0, 0, -9.81), damping=0.1),
        PyRunner(iterPeriod=10000, command = 'update()'),	# define function for loop processing at iteration number "iterPeriod"  
]

# set timestep to a fraction of the critical timestep
# the fraction is very small, so that the simulation is not too fast
# and the motion can be observed
N = 0
O.dt = .5e-4 * PWaveTimeStep()


yade.qt.Renderer().bgColor = (1,1,1)	#background color of 3D view = white

name = 'kido'

def update():
	global N	# If you want to use variables defined at main script keeping the same values in the function, please set the variables here.
	N += 0.001 
	if isinstance(O.bodies[1].shape,Sphere) == True:
		O.bodies[1].shape.color = (0.5,abs(O.bodies[1].state.displ()[2]),0.5)
		print(O.iter,abs(O.bodies[1].state.displ()[2]),N)	

# save the simulation, so that it can be reloaded later, for experimentation
O.saveTmp()

